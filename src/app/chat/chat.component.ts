import { Component, OnInit } from '@angular/core';
import { Channel } from '../ichannel';
import { ChatService } from '../chat.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  title = 'Tchat EPSI';

  channels: Channel[]
  constructor(private chatService: ChatService,
     private router: Router) { }

  ngOnInit() {
    this.chatService.getChannels().subscribe(
      (channels: Channel[]) => this.channels = channels
    )
  }

}
