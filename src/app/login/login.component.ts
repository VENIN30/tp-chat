import { Component, OnInit } from '@angular/core';
import { ChatService } from '../chat.service';
import { Router } from '@angular/router';
import { Channel } from '../ichannel';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title = 'Tchat EPSI';

  channels: Channel[]
  constructor(private chatService: ChatService,
     private router: Router) { }

  ngOnInit() {
    this.chatService.getChannels().subscribe(
      (channels: Channel[]) => this.channels = channels
    )
  }

  getPseudo() {
    this.router.navigate(['/chat']);
  }

}
