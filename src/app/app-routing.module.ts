import { NgModule } from '@angular/core';

import { LoginComponent } from './login/login.component';
import { ChatComponent } from './chat/chat.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'chat', component: ChatComponent } 
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }


