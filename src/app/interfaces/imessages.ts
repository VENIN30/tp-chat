export interface IChatMessage {
    message: string;
    username: string;
    userId: string;
  }